<?php

namespace Drupal\Tests\RegistrationRedirectTest\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the path validation on the settings page.
 *
 * @group redirect_after_registration
 */
class PathValidationTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'redirect_after_registration',
    'user',
    'test_page_test',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests the path validation on the settings page.
   */
  public function testAnonymousRegistrationDefaultLoginRedirect() {
    // Go to the settings page.
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/config/redirect_after_registration/config');
    $session = $this->assertSession();
    $session->statusCodeEquals(200);
    // First try out valid paths and check that they are accepted.
    $page->fillField('redirect', '');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContainsOnce('The configuration options have been saved.');
    $page->fillField('redirect', '/');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContainsOnce('The configuration options have been saved.');
    $page->fillField('redirect', '/admin');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContainsOnce('The configuration options have been saved.');
    $page->fillField('redirect', '/admin/config/');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContainsOnce('The configuration options have been saved.');
    $page->fillField('redirect', '<front>');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContainsOnce('The configuration options have been saved.');
    // Now, try out invalid paths and make sure that they are rejected.
    $page->fillField('redirect', 'abc-invalid-path');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContainsOnce('This path does not exist or you do not have permission to link to');
    $page->fillField('redirect', 'https://www.example.com');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContainsOnce('You cannot use an external URL. Enter a relative path.');
  }

}
