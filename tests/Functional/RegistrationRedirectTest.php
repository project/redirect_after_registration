<?php

namespace Drupal\Tests\RegistrationRedirectTest\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * This class provides methods specifically for testing something.
 *
 * @group project_wiki
 */
class RegistrationRedirectTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'redirect_after_registration',
    'user',
    'test_page_test',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);

    // Allow visitors user registration.
    $config = $this->config('user.settings');
    $config->set('register', UserInterface::REGISTER_VISITORS);
    $config->save();
  }

  /**
   * Tests the anonymous registration redirecting to the default setting.
   */
  public function testAnonymousRegistrationDefaultLoginRedirect() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Log out the user:
    $this->drupalLogout();
    $page->pressButton('edit-submit');
    // Register a new example user:
    $this->drupalGet('/user/register');
    $session->statusCodeEquals(200);
    $page->fillField('mail', 'info@example.com');
    $page->fillField('name', 'example-user');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->statusMessageContains('A welcome message with further instructions has been sent to your email address.');
    $session->pageTextContains('Log in');
    $this->assertSession()->addressEquals('/user/login');
  }

  /**
   * Tests the anonymous registration redirecting to a custom path.
   */
  public function testAnonymousRegistrationTestPageRedirect() {
    // Set test-page redirect:
    $config = $this->config('redirect_after_registration.settings');
    $config->set('redirect', '/test-page');
    $config->save();

    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Log out the user:
    $this->drupalLogout();
    $page->pressButton('edit-submit');
    // Register a new example user:
    $this->drupalGet('/user/register');
    $session->statusCodeEquals(200);
    $page->fillField('mail', 'info@example.com');
    $page->fillField('name', 'example-user');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->statusMessageContains('A welcome message with further instructions has been sent to your email address.');
    $session->pageTextContains('Test page');
    $this->assertSession()->addressEquals('/test-page');
  }

  /**
   * Tests the "Create user" registration by admins redirect to a custom path.
   */
  public function testAdminCreateUserRedirect() {
    $config = $this->config('redirect_after_registration.settings');
    $config->set('redirect', '/test-page');
    $config->set('redirect_admin_user_create', TRUE);
    $config->save();

    $session = $this->assertSession();
    $this->drupalLogin($this->adminUser);
    $page = $this->getSession()->getPage();
    // Register a new example user from the admin page.
    $this->drupalGet('/admin/people/create');
    $session->statusCodeEquals(200);
    $page->fillField('mail', 'info@example.com');
    $page->fillField('name', 'example-user');
    $page->fillField('pass[pass1]', 'example-password');
    $page->fillField('pass[pass2]', 'example-password');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->statusMessageContains('Created a new user account');
    $session->pageTextContains('Test page');
    $this->assertSession()->addressEquals('/test-page');
  }

  /**
   * Tests admin creation of users doesn't redirect if disabled.
   */
  public function testAdminCreateUserNoRedirect() {
    $config = $this->config('redirect_after_registration.settings');
    $config->set('redirect', '/test-page');
    $config->set('redirect_admin_user_create', FALSE);
    $config->save();

    $session = $this->assertSession();
    $this->drupalLogin($this->adminUser);
    $page = $this->getSession()->getPage();
    // Register a new example user from the admin page.
    $this->drupalGet('/admin/people/create');
    $session->statusCodeEquals(200);
    $page->fillField('mail', 'info@example.com');
    $page->fillField('name', 'example-user');
    $page->fillField('pass[pass1]', 'example-password');
    $page->fillField('pass[pass2]', 'example-password');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->statusMessageContains('Created a new user account');
    $session->pageTextNotContains('Test page');
    $this->assertSession()->addressNotEquals('/test-page');
    $this->assertSession()->addressEquals('/admin/people/create');
  }

}
