<?php

namespace Drupal\Tests\redirect_after_registration\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for redirect_after_registration.
 *
 * @group redirect_after_registration
 */
class GenericModuleTest extends GenericModuleTestBase {

  /**
   * {@inheritDoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. Just overwrite this method, so we don't have to
    // implement hook_help().
  }

}
