<?php

namespace Drupal\redirect_after_registration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\PathElement;

/**
 * Config form for setting redirect options after user registration.
 */
class RedirectAfterRegistrationConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'redirect_after_registration.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'redirect_after_registration_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $redirectConfig = $this->config('redirect_after_registration.settings');
    $form['redirect'] = [
      '#type' => 'path',
      '#title' => $this->t('Redirect path'),
      '#convert_path' => PathElement::CONVERT_NONE,
      '#description' => $this->t('Path to redirect the user to after submission of this form. For example, type "/about" to redirect to that page. Use a relative path with a slash in front. Leave empty to disable the redirect.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $redirectConfig->get('redirect'),
    ];

    $form['redirect_admin_user_create'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Redirect on administrative user creation'),
      '#description' => $this->t('Enables the redirect also for admin user creation (/admin/people/create). Typically you do not want that.'),
      '#default_value' => $redirectConfig->get('redirect_admin_user_create'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('redirect_after_registration.settings')
      ->set('redirect', $form_state->getValue('redirect') ? $form_state->getValue('redirect') : NULL)
      ->set('redirect_admin_user_create', $form_state->getValue('redirect_admin_user_create'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
